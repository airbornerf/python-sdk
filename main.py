import click
import logging
import json
from arf.ACJA2Client import ACJA2Client
from arf.Tile import Tile


logging.basicConfig(level=logging.DEBUG)

def showcaseGetConnectivityGeoJSON(api):
    """
    Aviation API showcase for getting a connectivity GeoJSON and reading the result
    :return:
    """

    # Start a "getVolume" request for connectivity (with GeoJSON result).
    serviceLevel = "C2"
    volume = {
        "id": "dfe56049-eddd-45e2-a973-40daefe4f91a",
        "type": "rectangle",
        "geometry": {
            "lat1": 47.3,
            "lat2": 47.39,
            "lon1": 16.2,
            "lon2": 16.29
        }
    }
    task_id = api.get_volume(volume, "connectivity", serviceLevel, [{"technology": "CELL_4G", "MCC": 1, "MNC": 1}], format="geojson")
    logging.info(f"getVolume success: taskId = {task_id}")

    # The background task now processes the request. Poll periodically until we get back a data ref.
    result = api.wait_for_task(task_id, timeout=180)
    dataRef = result['refs'][0]['ref']
    logging.info(f"pollTask success: dataRef = {dataRef}")

    # We successfully got a data ref back. Download the data file.
    api.download(dataRef, f"downloaded-geojson-{task_id}")

    # Verify it is a valid GeoJSON file
    with open(f"downloaded-geojson-{task_id}") as fp:
        data = json.load(fp)
        print(data['features'][0]['geometry'])


def showcaseGetConnectivityPointcloud(api):
    """
    Aviation API showcase for getting a connectivity pointcloud and reading the resulting volume
    (tile file).
    :return:
    """

    # Start a "getVolume" request for connectivity (with pointcloud result).
    serviceLevel = "C2"
    volume = {
        "id": "dfe56049-eddd-45e2-a973-40daefe4f91a",
        "type": "rectangle",
        "geometry": {
            "lat1": 47.3,
            "lat2": 47.39,
            "lon1": 16.2,
            "lon2": 16.29
        }
    }
    task_id = api.get_volume(volume, "connectivity", serviceLevel, [{"technology": "CELL_4G", "MCC": 1, "MNC": 1}])
    logging.info(f"getVolume success: taskId = {task_id}")

    # The background task now processes the request. Poll periodically until we get back a data ref.
    result = api.wait_for_task(task_id, timeout=180)
    dataRef = result['refs'][0]['ref']
    logging.info(f"pollTask success: dataRef = {dataRef}")

    # We successfully got a data ref back. Download the data file.
    api.download(dataRef, f"downloaded-tile-{task_id}")

    # Now we can open the data file and verify its integrity
    tile = Tile(f"downloaded-tile-{task_id}")
    assert tile.validateChecksum()

    # Now query a few locations and get the connectivity state there.

    # Convert a certain 3D coordinate (latitude, longitude, altitude) to tile coordinates (x, y, z):
    # Note: you need to make sure that you are within the boundaries of the tile. (check against tile.getHeader() fields)
    latitude = 47.361
    longitude = 16.2112
    altitude = 590
    x, y, z = tile.getTileCoordinates(latitude, longitude, altitude)
    value = tile.getNode(x, y, z)
    if value is None:
        logging.info(f"No connectivity information available at that coordinate")
    elif value == 0:
        logging.info(f"No connectivity for service level {serviceLevel}")
    elif value == 1:
        logging.info(f"Marginal connectivity for service level {serviceLevel}")
    elif value == 2:
        logging.info(f"Good connectivity for service level {serviceLevel}")

def showcaseAnalyzeOperationPlan(api):
    """
    Aviation API showcase for analyzing an operation plan (flight path).
    :return:
    """

    # Start a "analyzeOperationPlan" request for connectivity.
    serviceLevel = "C2"

    operationPlan = {
            "gufi": "edb829e6-4f65-4fdf-a3da-aa482597eb81",
            "aquisitionDateTimeStart": "2023-05-16T07:42:26Z",
            "trajectoryElements": [
                [47.7412,16.0884,500,0,0,0,0],
                [47.7413,16.0886,500,0,0,0,0.0546448],
                [47.7414,16.0887,500,0,0,0,0.0546448],
                [47.7414,16.0888,500,0,0,0,0.0546448],
                [47.7415,16.089,500,0,0,0,0.0546448],
                [47.7416,16.0891,500,0,0,0,0.0546448],
                [47.7417,16.0893,500,0,0,0,0.0546448],
                [47.7418,16.0894,500,0,0,0,0.0546448],
                [47.7419,16.0896,500,0,0,0,0.0546448],
                [47.742,16.0897,500,0,0,0,0.0546448],
                [47.7421,16.0898,500,0,0,0,0.0546448],
                [47.7422,16.09,500,0,0,0,0.0546448],
                [47.7423,16.0901,500,0,0,0,0.0546448],
                [47.7424,16.0903,500,0,0,0,0.0546448],
                [47.7425,16.0904,500,0,0,0,0.0546448],
                [47.7426,16.0906,500,0,0,0,0.0546448],
                [47.7427,16.0907,500,0,0,0,0.0546448],
                [47.7428,16.0908,500,0,0,0,0.0546448],
                [47.7429,16.091,500,0,0,0,0.0546448],
                [47.743,16.0911,500,0,0,0,0.0546448],
                [47.7431,16.0913,500,0,0,0,0.0546448],
                [47.7431,16.0914,500,0,0,0,0.0546448],
                [47.7432,16.0916,500,0,0,0,0.0546448],
                [47.7433,16.0917,500,0,0,0,0.0546448],
                [47.7434,16.0918,500,0,0,0,0.0546448],
                [47.7435,16.092,500,0,0,0,0.0546448],
                [47.7436,16.0921,500,0,0,0,0.0546448],
                [47.7437,16.0923,500,0,0,0,0.0546448],
                [47.7438,16.0924,500,0,0,0,0.0546448],
                [47.7439,16.0926,500,0,0,0,0.0546448],
                [47.744,16.0927,500,0,0,0,0.0546448],
                [47.7441,16.0928,500,0,0,0,0.0546448],
                [47.7442,16.093,500,0,0,0,0.0546448],
                [47.7443,16.0931,500,0,0,0,0.0546448],
                [47.7444,16.0933,500,0,0,0,0.0546448],
                [47.7445,16.0934,500,0,0,0,0.0546448],
                [47.7446,16.0936,500,0,0,0,0.0546448],
                [47.7447,16.0937,500,0,0,0,0.0546448],
                [47.7448,16.0938,500,0,0,0,0.0546448],
                [47.7449,16.094,500,0,0,0,0.0546448],
            ],
            "safetyBufferHorizontal": 60,
            "safetyBufferVertical": 60,
            "radioParameters": [
                {
                    "technology": "CELL_4G",
                    "antenna": [
                        {
                            "type": "isotropic"
                        }
                    ]
                }
            ]
    }
    task_id = api.analyze_operation_plan(operationPlan, "connectivity", serviceLevel, [{"technology": "CELL_4G", "MCC": 1, "MNC": 1}])
    logging.info(f"analyzeOperationPlan success: taskId = {task_id}")

    # The background task now processes the request. Poll periodically until we get back a result.
    # Please consult the documentation on how to interpret the "result" string.
    result = api.wait_for_task(task_id, timeout=180)
    logging.info(f"pollTask success: result = {result['result']}")


def showcaseSubscribeVolume(api):
    volume = {
        "id": "",
        "type": "tile",
        "tilespec": "n470e0160"
    }
    kind = "connectivity"
    service_level = "C2"
    callback_url = "http://localhost:18081/subscription/callback"
    callback_method = "HTTP-POST"
    user_data = {
        "key1": "value1"
    }
    connectivity_providers = [
        {
            "technology": "CELL_4G",
            "MCC": 1,
            "MNC": 1
        }
    ]

    subscription_id = api.subscribe_volume(
        volume,
        kind,
        service_level,
        connectivity_providers,
        callback_url,
        callback_method,
        user_data
    )

    logging.info(f"successfully created subscription with id {subscription_id}")


@click.command()
@click.argument('endpoint-url')
@click.argument('auth-token')
def main(endpoint_url, auth_token):
    """
    This is a CLI tool for the Aviation API.

    \b
    ENDPOINT_URL: The Aviation API endpoint URL
    AUTH_TOKEN: The Aviation API auth token
    """
    api = ACJA2Client(endpoint_url, auth_token)

    showcaseGetConnectivityPointcloud(api)
    showcaseGetConnectivityGeoJSON(api)
    showcaseAnalyzeOperationPlan(api)
    showcaseSubscribeVolume(api)


if __name__ == '__main__':
    main()
