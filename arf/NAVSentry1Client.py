from typing import Optional

import requests
import logging
import json
import datetime
import time


class NAVSentry1Client:
	server_url = None
	xsrf_token = None
	access_token = None
	logger = logging.getLogger("airbornerf.NAVSentry1Client")

	def __init__(self, server_url, access_token):
		self.server_url = server_url
		self.session = requests.Session()
		self.access_token = access_token
		self.logger.setLevel(logging.DEBUG)

	def _response_check(self, response):

		if response.status_code != requests.codes.ok: #pylint: disable=no-member
			self.logger.error("Request failed: HTTP " + str(response.status_code))
			self.logger.error(response.text)
			raise RuntimeError("API request failed: HTTP " + str(response.status_code))

	def _response_check_json(self, response):

		self._response_check(response)
		jesponse = response.json()
		if jesponse['success'] != True:
			self.logger.error("Request failed: success is False")
			self.logger.error(jesponse)
			raise RuntimeError("API request failed: {} ({})".format(jesponse['errorMessage'], jesponse['errorCode']))
		return jesponse

	def get_active_warning_areas(self):
		headers = {
			'cache-control': "no-cache",
			'Authorization': self.access_token,
			'Content-Type': 'application/json'
		}
		response = self.session.request("GET", self.server_url + "/navsentry/v1.00/getActiveWarningAreas", headers=headers)
		self._response_check(response)
		return response.json()