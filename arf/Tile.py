import copy
import hashlib
import logging
import math
import mmap
import time
from collections import namedtuple
from math import floor
from struct import pack, unpack
from typing import Final, Tuple

import numpy as np
import numpy.typing as npt

from arf.TileConstants import TileConstants


class Tile:
	logger = logging.getLogger("Tile")

	TileHeader = namedtuple('TileHeader',
							'magic version xsize ysize zsize altitude verticalResolution latitude longitude latSize lonSize '
							'dtype valueOffset creationTime ceiling checksum')

	class CalculatedTileHeader:
		horizontalLayerSize : int = 0
		tileBufferLength :int = 0

	# Member variables
	header = None
	calculatedHeader = None
	fp = None
	mm = None

	headerOffset: Final[int] = TileConstants.headerOffset

	def __init__(self, tileFilename: str=None, longitude: float=None, latitude: float=None,
				 xsize: int = None, ysize: int=None, zsize: int=None, altitude:int=None,
				 verticalResolution:int=None, dtype:int=None, valueOffset:int=0,
				 tile=None, latSize:float=0.1, lonSize:float=0.1, data:npt.ArrayLike=None):

		if tileFilename is not None:
			self.fp = open(tileFilename, 'rb')
			self.mm = mmap.mmap(self.fp.fileno(), 0, access=mmap.ACCESS_READ)

			# Read the header
			self.header = self.TileHeader._make(unpack(TileConstants.headerFieldSizes, self.mm[0:TileConstants.actualHeaderSize]))
			if self.header.magic != b'DMTRTILE':
				raise RuntimeError("File is not a Dimetor tile!")
			if self.header.version > 4:
				# IMPORTANT: NOTE: If you raise the Tile version number, also check and change it in
				# the "cell-importer" project where a shortcut is used for performance reasons! Search
				# for "tile.header.version" there, and you'll find it. It should be a simple fix, but must not
				# be forgotten. A few PD importers use that important shortcut.
				raise RuntimeError("Unsupported Dimetor tile version: {}!".format(self.header.version))

			if self.header.dtype != 1 and self.header.dtype != 4 and self.header.dtype != 8 and self.header.dtype != 16:
				raise RuntimeError(f'Dtype {self.header.dtype} is not supported - only node dtype of 1, 4, 8 or 16 are currently supported!')

			self._calculateHelpers()

		elif tile is not None:
			# Construct a Tile from another tile (= 1:1 cloning)
			self.header = copy.deepcopy(tile.header)
			self._calculateHelpers()
			self.mm = bytearray(tile.mm[:])
			self.writeHeaderToBuffer()
		else:
			# create a new Tile in memory.
			if data is not None:
				if data.ndim == 2:
					data_x = data.shape[1]
					data_y = data.shape[0]
					data_z = 1
				elif data.ndim == 3:
					data_z, data_y, data_x = data.shape
				else:
					raise ValueError("Data must be 2d or 3d array")

				if xsize is None:
					xsize = data_x
				if ysize is None:
					ysize = data_y
				if zsize is None:
					zsize = data_z

				if xsize != data_x or ysize != data_y or zsize != data_z:
					raise ValueError("Array shape does not match tile size")

			header = {
				'magic': b'DMTRTILE',
				'version': 4,
				'xsize': xsize,
				'ysize': ysize,
				'latSize': latSize,
				'lonSize': lonSize,
				'verticalResolution': verticalResolution,
				'altitude': altitude,
				'zsize': zsize,
				'latitude': latitude,
				'longitude': longitude,
				'dtype': dtype,
				'valueOffset': valueOffset,
				'creationTime': int(time.time()),
				'checksum': bytearray(20),
				'ceiling': 0
				# 'padding': bytearray(512)
			}
			self.header = self.TileHeader(**header)

			if self.header.dtype != 1 and self.header.dtype != 4 and self.header.dtype != 8 and self.header.dtype != 16:
				raise RuntimeError("Only node dtypes of 1, 4, 8 or 16 are currently supported!")

			self._calculateHelpers()

			# create the buffer
			self.mm = bytearray(self.calculatedHeader.tileBufferLength)
			self.writeHeaderToBuffer()

			if data is not None:
				self.writeArray(data)

		self.logger.debug("magic = {}".format(self.header.magic))
		self.logger.debug("version = {}".format(self.header.version))
		self.logger.debug("xsize = {}".format(self.header.xsize))
		self.logger.debug("ysize = {}".format(self.header.ysize))
		self.logger.debug("zsize = {}".format(self.header.zsize))
		self.logger.debug("altitude = {}".format(self.header.altitude))
		self.logger.debug("verticalResolution = {}".format(self.header.verticalResolution))
		self.logger.debug("latitude = {}".format(self.header.latitude))
		self.logger.debug("longitude = {}".format(self.header.longitude))
		self.logger.debug("latSize = {}".format(self.header.latSize))
		self.logger.debug("lonSize = {}".format(self.header.lonSize))
		self.logger.debug("dtype = {}".format(self.header.dtype))
		self.logger.debug("valueOffset = {}".format(self.header.valueOffset))
		self.logger.debug("creationTime = {}".format(self.header.creationTime))
		self.logger.debug("ceiling = {}".format(self.header.ceiling))
		self.logger.debug("checksum = {}".format(self.header.checksum))

	@staticmethod
	def calculate_n_bytes(nodes: int, node_density: int) -> int:
		"""
		Computes the number of bytes that are needed to store the data for the node values.
		We define node density as log2(n_bytes_node),
		where n_bytes_node is the number of bytes that are needed to store the data for one node.

		:param nodes: The number of nodes.
		:param node_density: The node density.
		:return: Number of bytes that are needed to store the data for the nodes.
		"""
		if node_density > 0:
			return nodes * (1 << node_density)
		elif node_density < 0:
			n_bytes = nodes // (1 << -node_density)
			r = nodes % (1 << -node_density)
			return n_bytes if r == 0 else n_bytes + 1
		else:
			return nodes

	def _calculateHelpers(self):
		self.calculatedHeader = self.CalculatedTileHeader()
		self.calculatedHeader.horizontalLayerSize = self.header.xsize * self.header.ysize
		number_of_grid_nodes = self.calculatedHeader.horizontalLayerSize * self.header.zsize
		header_dtype_to_density_map = {1: -3,
									   4: -1,
									   8: 0,
									   16: 1}

		node_data_bytes = Tile.calculate_n_bytes(number_of_grid_nodes, header_dtype_to_density_map[self.header.dtype])
		self.calculatedHeader.tileBufferLength = node_data_bytes + TileConstants.headerOffset

		self.logger.debug(self.calculatedHeader.horizontalLayerSize)
		self.logger.debug(self.calculatedHeader.tileBufferLength)

	def writeHeaderToBuffer(self):
		self.mm[0:TileConstants.actualHeaderSize] = pack(TileConstants.headerFieldSizes, *self.header)

	@staticmethod
	def roundHalfUp(x):
		floorX = floor(x)
		if x - floorX >= 0.5:
			return floorX + 1
		return floorX

	def findCoordinates(self, lat: float, lon: float) -> (int, int, int):
		"""
		Find the z layer where the node value at the given coordinate is 0 for the first time.
		:param lat:
		:param lon:
		:return:
		"""
		x = self.roundHalfUp((lon - self.header.longitude) / self.header.lonSize * self.header.xsize)
		y = self.roundHalfUp((self.header.latitude - lat) / self.header.latSize * self.header.ysize)

		for z in range(0, self.header.zsize):
			if self.getNode(x, y, z) == 0:
				return x, y, z

		return -1, -1, -1

	@staticmethod
	def clamp(value, min_value, max_value):
		return min(max(min_value, value), max_value)

	@staticmethod
	def getTileCoordinatesWithHeader(header, lat: float, lon: float, altitude: float, clip_coo = True) -> (int, int, int):
		"""
		Convert WGS 84 coordinates to tile coordinates
		:param lat:
		:param lon:
		:param altitude:
		:param clip_coo: If true, coordinates are clipped to valid range ( s.t. (x,y,z) corresponds to a point on the tile area).
		:return:
		"""
		x = Tile.roundHalfUp((lon - header.longitude) / header.lonSize * header.xsize)
		y = Tile.roundHalfUp((header.latitude - lat) / header.latSize * header.ysize)
		z = Tile.roundHalfUp((altitude - header.altitude) / header.verticalResolution)

		if clip_coo:
			x = Tile.clamp(x, 0, header.xsize - 1)
			y = Tile.clamp(y, 0, header.ysize - 1)
			z = Tile.clamp(z, 0, header.zsize - 1)

		return x, y, z

	def getTileCoordinates(self, lat: float, lon: float, altitude: float, clip_coo = True) -> (int, int, int):
		return Tile.getTileCoordinatesWithHeader(self.header, lat, lon, altitude, clip_coo)

	@staticmethod
	def getFractionalCoordinatesWithHeader(header, lat: float, lon: float, clip_coo= True) -> (float, float):
		"""
		Convert WGS 84 coordinates to fractional tile coordinates
		:param lat:
		:param lon:
		:return:
		"""
		x = (lon - header.longitude) / header.lonSize * header.xsize
		y = (header.latitude - lat) / header.latSize * header.ysize

		if clip_coo:
			x = Tile.clamp(x, 0, header.xsize - 1)
			y = Tile.clamp(y, 0, header.ysize - 1)

		return x, y

	def getLatLong(self, x: float, y: float) -> (float, float):
		"""
		Convert tile coordinates to WGS 84 coordinates
		:param x:
		:param y:
		:return:
		"""

		lon = self.header.longitude + x / self.header.xsize * self.header.lonSize
		lat = self.header.latitude - y / self.header.ysize * self.header.latSize
		return lon, lat

	def getLatLongAlt(self, x: int, y: int, z: int) -> (float, float, float):
		"""
		Convert tile coordinates to WGS 84 coordinates
		:param x:
		:param y:
		:param z:
		:return:
		"""
		lon = x / self.header.xsize * self.header.lonSize + self.header.longitude
		lat = self.header.latitude - y / self.header.ysize * self.header.latSize
		altitude = z * self.header.verticalResolution + self.header.altitude
		return lon, lat, altitude

	def getNode(self, x: int, y: int, z: int):

		if x >= self.header.xsize or y >= self.header.ysize or z >= self.header.zsize or x < 0 or y < 0 or z < 0:
			return None

		node_index = self._getNodeIndex(x, y, z)

		if self.header.dtype == 1:
			byte_index = int(node_index / 8)
			bit_index = node_index % 8
			byte_index += TileConstants.headerOffset
			return (self.mm[byte_index] & (1 << bit_index)) >> bit_index
		elif self.header.dtype == 4:
			byte_index = int(node_index / 2)
			nibble_index = node_index % 2
			byte_index += TileConstants.headerOffset
			bufVal = (self.mm[byte_index] & (0xF0 >> (nibble_index * 4))) >> ((1 - nibble_index) * 4)
			if bufVal == 0:
				return None
			else:
				return bufVal + self.header.valueOffset
		elif self.header.dtype == 8:
			byte_index = node_index + TileConstants.headerOffset
			bufVal = self.mm[ byte_index ]
			if bufVal == 0:
				return None
			else:
				return bufVal + self.header.valueOffset
		elif self.header.dtype == 16:
			byte_index = node_index * 2 + TileConstants.headerOffset
			bufVal = ( ( self.mm[ byte_index ] << 8 ) | (self.mm[ byte_index + 1 ] ) ) & 0xFFFF
			if bufVal == 0:
				return None
			else:
				return bufVal + self.header.valueOffset
		else:
			raise RuntimeError("Unsupported dtype")

	def putNode(self, value: int, x: int, y: int, z: int):
		"""
		Put the value at the specified location into the tile map.
		Pass None to indicate no value
		:param value:
		:param x:
		:param y:
		:param z:
		:return:
		"""
		if x >= self.header.xsize or y >= self.header.ysize or z >= self.header.zsize:
			return
		if x < 0 or y < 0 or z < 0:
			return

		node_index = self._getNodeIndex(x, y, z)

		if self.header.dtype == 1:
			byte_index = int(node_index / 8)
			bit_index = node_index % 8
			byte_index += TileConstants.headerOffset

			# Note: this is not thread safe! Would need atomic ANDs and ORs.
			if value != 0:
				bitmask = 1 << bit_index
				self.mm[byte_index] = self.mm[byte_index] | bitmask
			else:
				bitmask = ~(1 << bit_index)
				self.mm[byte_index] = self.mm[byte_index] & bitmask
		elif self.header.dtype == 4:
			# Note: this is not thread safe! Would need atomic ANDs and ORs.
			byte_index = int(node_index / 2)
			nibble_index = node_index % 2
			byte_index += TileConstants.headerOffset
			theValue = 0
			if value is not None:
				val = value - self.header.valueOffset
				theValue = max(min(16, val), 1)
			self.mm[byte_index] &= 0x0F << (nibble_index * 4)
			self.mm[byte_index] |= (theValue & 0x0F) << ((1-nibble_index) * 4)
		elif self.header.dtype == 16:
			byte_index = node_index * 2 + TileConstants.headerOffset
			theValue = 0
			if value is not None:
				val = value - self.header.valueOffset
				theValue = max(min(65535, val), 1)
			self.mm[byte_index] = (theValue >> 8) & 0xFF
			self.mm[byte_index + 1] = theValue & 0xFF
		else:
			raise RuntimeError(f'Unsupported dtype of {self.header.dtype}.')

	def getZElevation(self, x: int, y: int):
		"""
		Calculates the Z elevation at the given point.
		:param x:
		:param y:
		:return:
		"""
		for z in range(0, self.header.zsize):
			if self.getNode(x, y, z) == 0:
				return z

	def getElevation(self, x: int, y: int):
		"""
		Returns elevation at the given point.
		:param x:
		:param y:
		:return:
		"""
		return self.getZElevation(x, y) * self.header.verticalResolution + self.header.altitude

	def __del__(self):

		if type(self.mm) is mmap.mmap:
			self.mm.close()
		if self.fp is not None:
			self.fp.close()

	# TODO: check if getLayerZ is needed
	def getLayerZ(self, z: int):
		"""
		Extract layer points and return as 2D array.
		:param z:
		:return:
		"""
		points = []
		for x in range(0, self.header.xsize):
			column = []
			for y in range(0, self.header.ysize):
				column.append(self.getNode(x, y, z))
			points.append(column)
		return points
	def _getNodeIndex(self, x:int, y:int, z:int):
		return z * self.calculatedHeader.horizontalLayerSize + y * self.header.xsize + x

	def getTileLayers(self, returnAsList, return2D, *layers):
		"""
        Extract all point values of a layer and return a list of 1D/2D arrays OR a 3D array.
		Caveat: Coordinate order of return-value-array: z-y-x
		:return:
		"""

		dtype = TileConstants.DTypes(self.header.dtype)

		if dtype == TileConstants.DTypes.UINT1:
			values = np.unpackbits(self.mm, bitorder='little')
			values = values[TileConstants.headerOffset * 8:]

		elif dtype == TileConstants.DTypes.UINT4:
			node_buffer = self.mm[TileConstants.headerOffset:]
			node_buffer = np.frombuffer(node_buffer, dtype=np.uint8)

			val_even = np.bitwise_and(node_buffer >> 4 , 0x0f)
			val_uneven = np.bitwise_and(node_buffer, 0x0f)
			values = np.dstack((val_even, val_uneven)).flatten().astype(np.int8) + self.header.valueOffset

		elif dtype == TileConstants.DTypes.UINT8:
			node_buffer = self.mm[TileConstants.headerOffset:]
			values = np.frombuffer(node_buffer, dtype=np.uint8).astype(np.int16) + self.header.valueOffset

		elif dtype == TileConstants.DTypes.UINT16:
			node_buffer = self.mm[TileConstants.headerOffset:]
			values = np.frombuffer(node_buffer, dtype=">u2").astype(np.int32) + self.header.valueOffset

		elif dtype == TileConstants.DTypes.BFLOAT16:
			node_buffer = self.mm[TileConstants.headerOffset:]
			values = np.frombuffer(node_buffer, dtype=">f2") + self.header.valueOffset

		else:
			raise RuntimeError("Unsupported dtype")

		if not layers:
			layers = range(self.header.zsize)

		extractedLayers = []

		for z in layers:
			# Layer extracted as 1D array
			node_index_start = self._getNodeIndex(0, 0, z)
			node_index_end = self._getNodeIndex(0, 0, z + 1)

			layer = values[node_index_start: node_index_end]
			if return2D:
				extractedLayers.append(layer.reshape(self.header.ysize, self.header.xsize))
			else:
				extractedLayers.append(layer)

		if returnAsList:
			return extractedLayers
		else:
			return np.asarray(extractedLayers)


	# TODO: check if getNodeLayerZ is needed
	def getNodeLayerZ(self, value: int, z: int):
		"""
		Extract all the points in a layer with a specific value (ex. 0 or 1)
		:param value:
		:param z:
		:return:
		"""
		points = []
		for x in range(0, self.header.xsize):
			for y in range(0, self.header.ysize):
				if self.getNode(x, y, z) == value:
					points.append([x, y])
		npArray = np.asarray(points)
		return npArray

	# TODO: check if getNodeLayerZset is needed
	def getNodeLayerZset(self, value: int, z: int):
		""""
		Extract all the points in a layer with a specific value (ex. 0 or 1) but return set.
		:param value:
		:param z:
		:return:
		"""
		pSet = set()
		for x in range(0, self.header.xsize):
			for y in range(0, self.header.ysize):
				if self.getNode(x, y, z) == value:
					pSet.add((x, y))
		return pSet

	def getLayerHeight(self, z: int):
		"""
		Get the height of a layer passing the z layer index.
		:param z:
		:return:
		"""
		return self.header.altitude + z * self.header.verticalResolution

	def getLayerFloorAndCeiling(self, z: int) -> (float, float):
		"""
		Get the bottom and top of a layer passing the z layer index.
		:param z:
		:return:
		"""
		height = self.getLayerHeight(z)

		floor = height - 0.5 * self.header.verticalResolution
		ceiling = height + 0.5 * self.header.verticalResolution

		return floor, ceiling

	@staticmethod
	def normalizeCoordinates(latitude: float, longitude: float) -> (float, float):
		"""
		Fix up input lat/lon to be within [-90,90] and [-180,180) respectively.
		This avoids problems if out of range coordinates are given anywhere (e.g. as user input).

		Longitude wraps around at 180 to -180. The reason for choosing this range
		(instead of (-180,180]) is that the tilespec represents the SW corner, so
		the tile comprises points "to the right" of its name - this way they have
		the same sign.

		Latitudes >90 and <-90 are treated as being mirrored at +-90, with the longitude
		being "reversed" in this case - as if one walked straight over
		the North or South Pole.

		:param latitude:
		:param longitude:
		:return: tupple (latitude, longitude) with the normalized coordinates.
		"""
		if latitude >= 180 or latitude < -180:
			latitude = (math.fmod((math.fmod(latitude + 180.0, 360.0) + 360.0), 360.0) - 180.0)

		if latitude >= 90:
			latitude = 180 - latitude
			longitude += 180
		elif latitude < -90:
			latitude = -180 - latitude
			longitude += 180

		if longitude >= 180 or longitude < -180:
			longitude = (math.fmod((math.fmod(longitude + 180, 360) + 360), 360) - 180)

		return latitude, longitude

	@staticmethod
	def generateTilespec(latitude: float, longitude: float) -> str:
		"""
		Generate the tilespec for the tile that contains the location at lat, lon
		:param lat:
		:param lon:
		:return:
		"""

		latitude, longitude = Tile.normalizeCoordinates(latitude, longitude)

		assert -90 <= latitude <= 90, "Latitude out of bounds"
		assert -180 <= longitude <= 180, "Longitude out of bounds"

		tilespec = [''] * 9

		tilespec[0] = 'n' if latitude >= 0 else 's'
		tilespec[4] = 'e' if longitude >= 0 else 'w'

		lat_deci_deg = int(abs(floor(latitude * 10)))
		q = lat_deci_deg // 100
		r = lat_deci_deg % 100

		tilespec[1] = chr(q + ord('0'))
		q = r // 10
		r = r % 10
		tilespec[2] = chr(q + ord('0'))
		tilespec[3] = chr(r + ord('0'))

		lon_deci_deg = int(abs(floor(longitude * 10)))
		q = lon_deci_deg // 1000
		r = lon_deci_deg % 1000

		tilespec[5] = chr(q + ord('0'))

		q = r // 100
		r = r % 100

		tilespec[6] = chr(q + ord('0'))

		q = r // 10
		r = r % 10

		tilespec[7] = chr(q + ord('0'))
		tilespec[8] = chr(r + ord('0'))

		return ''.join(tilespec)

	@staticmethod
	def _generateTilespec1Deg( lat_floor_abs :int , n_s: chr, lon_floor_abs: int, w_e: chr) -> str:

		"""
		Returns the one-degree-tilespec for the given grid point (+/- lat_floor_abs, +/- lon_floor_abs).
		The coordinates of the grid point must be non-negative,
		the sign of the coordinates is specified by the character n_s and w_e .

		:param lat_floor_abs: Specifies the latitude component of the tilespec. Must be non-negative.
		:param n_s: must be either 'n' or 's'
		:param lon_floor_abs:  Specifies the longitude component of the tilespec. Must be non-negative.
		:param w_e: must be either 'w' or 'e'
		:return:
		"""


		lat, lon = lat_floor_abs, lon_floor_abs

		tilespec = [''] * 7

		tilespec[0] = n_s
		tilespec[1] = chr((lat//10) + ord('0'))
		tilespec[2] = chr((lat%10) + ord('0'))

		tilespec[3] = w_e
		tilespec[4] = chr((lon//100) + ord('0'))
		tilespec[5] = chr(((lon//10)%10) + ord('0'))
		tilespec[6] = chr((lon%10) + ord('0'))

		return ''.join(tilespec)

	@staticmethod
	def generateTilespec1Deg(latitude: float, longitude: float) -> str:
		"""
		Generate the one-degree tilespec for the tile that contains the location at lat, lon
		:param lat:
		:param lon:
		:return:
		"""

		latitude, longitude = Tile.normalizeCoordinates(latitude, longitude)

		assert -90 <= latitude <= 90, "Latitude out of bounds"
		assert -180 <= longitude <= 180, "Longitude out of bounds"

		return Tile._generateTilespec1Deg(int(np.abs(np.floor(latitude))), 's' if latitude < 0 else 'n', int(np.abs(np.floor(longitude))), 'w' if longitude < 0 else 'e')

	@staticmethod
	def generateTilespec1DegForTilespec(tilespec: str):

		"""
		Returns the one-degree-tilespec for the given (centi)-tilespec
		(i.e. returns the identifier of the 1x1-area which contains the 0.1 x 0.1-degree-area given by the (centi-)tilespec.
		:param tilespec: (centi)-tilespec.
		:return:
		"""

		lat_floor_abs = int(tilespec[1]) * 10 + int(tilespec[2])

		n_s = tilespec[0]
		e_w = tilespec[4]

		if n_s == 's' or n_s == 'S':
			if int(tilespec[3]) > 0:
				lat_floor_abs += 1

		lon_floor_abs = int(tilespec[5]) * 100 + int(tilespec[6]) * 10 + int(tilespec[7])

		if e_w == 'w' or e_w == 'W':
			if int(tilespec[8]) > 0:
				lon_floor_abs += 1

		return Tile._generateTilespec1Deg(lat_floor_abs, n_s, lon_floor_abs, e_w)


	@staticmethod
	def get1DegTileReferenceSystemForTilespec(tilespec: str) -> Tuple[str, int, int]:
		"""
		For the given tilespec, the one-degree-tilespec ts_1_deg of the tile area T´ on which the centi-tile
		area T is located as well as the sub-tile-index-position (x,y) of T wrt to T´ is computed.
		The sub-tile-index-position (x,y), with x, y in [0,1,...,9] refers to the reference point of the tilespec
		(the lower-left corner of T´), and increases with increasing longitude/latitude values.
		The x-coordinate refers to longitudal direction, whereas the y-coordinate refers to latitudal direction.
		In particular,
		- (0,0) is the south-western (centi)-tile of T'.
		- (9,0) is the south-eastern (centi)-tile of T'.
		- (0,9) is the north-western (centi)-tile of T'.
		- (9,9) is the north-eastern (centi)-tile of T'.

		Note that for tilespecs northern of the equator and eastern to the prime-meridian,
		the centi-tile-index coincides with the decimal-coordinates of the centi-tile area
		(identically, the first decimal place of the coordinates of the lower left corner that identifies the
		(centi)-tile area). So (nABYeFGHX) -- (nABeFGH, X, Y).

		Ex:
		- (n487e165) --> (n48e16, 5, 7)
		- (s500e165) --> (s50e16, 5, 0)
		- (s499e165) --> (s50e16, 5, 1)

		:param tilespec:
		:return: ts_1deg, x, y
		"""

		y = int(tilespec[3])
		x = int(tilespec[8])
		y = 10 - y if (tilespec[0] == 's' and y > 0) else y
		x = 10 - x if (tilespec[4] == 'w' and x > 0) else x
		return Tile.generateTilespec1DegForTilespec(tilespec), x, y

	@staticmethod
	def _getCoordinatesForTilespec(tilespec: str) -> (float, float):
		latitude = int(tilespec[1]) * 10 + int(tilespec[2]) + int(tilespec[3]) * 0.1

		if tilespec[0] == 's' or tilespec[0] == 'S':
			latitude *= -1

		longitude = int(tilespec[5]) * 100 + int(tilespec[6]) * 10 + int(tilespec[7]) + int(tilespec[8]) * 0.1

		if tilespec[4] == 'w' or tilespec[4] == 'W':
			longitude *= -1

		return latitude, longitude

	@staticmethod
	def getCoordinatesForTilespec1Deg(tilespec: str) -> (float, float):
		latitude = int(tilespec[1]) * 10 + int(tilespec[2])

		if tilespec[0] == 's' or tilespec[0] == 'S':
			latitude *= -1

		longitude = int(tilespec[4]) * 100 + int(tilespec[5]) * 10 + int(tilespec[6])

		if tilespec[3] == 'w' or tilespec[4] == 'W':
			longitude *= -1

		return latitude, longitude

	@staticmethod
	def getCoordinatesForTilespec(tilespec):
		if len(tilespec) == 9:
			return Tile._getCoordinatesForTilespec(tilespec)
		elif len(tilespec) == 7:
			return Tile.getCoordinatesForTilespec1Deg(tilespec)
		else:
			raise RuntimeError(f"Cannot process the tilespec {tilespec}.")
	def validateChecksum(self):
		"""
		Checks if the checksum stored in the "checksum" header is correct for this tile.
		:return:
		"""
		sha1 = hashlib.sha1()

		# Update from the start of the buffer to the checksum:
		sha1.update(self.mm[0:50])

		# Next, add a fake checksum field with all zeros
		sha1.update(b'\x00' * 20)

		# Finally, add everything from after the checksum to the end of file
		sha1.update(self.mm[70:])

		return sha1.digest() == self.header.checksum

	def createChecksum(self):
		sha1 = hashlib.sha1()

		# Update from the start of the buffer to the checksum:
		sha1.update(self.mm[0:50])

		# Next, add a fake checksum field with all zeros
		sha1.update(b'\x00' * 20)

		# Finally, add everything from after the checksum to the end of file
		sha1.update(self.mm[70:])

		self.mm[50:70] = sha1.digest()
		self.header.checksum[0:20] = sha1.digest()

	def writeToFile(self, filename):
		self.createChecksum()
		with open(filename, 'wb') as fp:
			fp.write(self.mm)

	def extractCentiTilePart(self, centi_tilespec: str) -> np.ndarray:
		tilespec_1_deg, x, y = Tile.get1DegTileReferenceSystemForTilespec(centi_tilespec)
		this_tilespec_1_deg = self.generateTilespec1Deg(latitude=self.header.latitude - 0.5, longitude=self.header.longitude + 0.5)
		if tilespec_1_deg != this_tilespec_1_deg:
			raise RuntimeError(f"Inconsistent one-degree tilespec of sub-tile {centi_tilespec} (with 1-deg ts {tilespec_1_deg} ) and main tile {this_tilespec_1_deg}.")

		return self.extractPart(x=x, y=y, divideBy=10)
	def extractPart(self, x: int, y: int, divideBy: int=10) -> np.ndarray:
		"""
		Extract a part of a tile (e.g. a centitile)
		x, y arguments count from the bottom left corner
		e.g. as used in the n__Ye___X centi-tilespec
		"""
		if self.header.xsize % divideBy or self.header.ysize % divideBy:
			raise RuntimeError("Tile dimensions not divisible by divideBy")

		array = self.getTileLayers(returnAsList=False, return2D=True)
		xsize = self.header.xsize // divideBy
		ysize = self.header.ysize // divideBy

		slice_x = slice(xsize*x, xsize*x + xsize)
		y = divideBy - 1 - y
		slice_y = slice(ysize*y, ysize*y + ysize)

		return array[:, slice_y, slice_x]

	def writeArray(self, data: npt.ArrayLike):
		"""
		Write a numpy array to the tile buffer.
		Constraints:
		* the shape must match the tile's size
		* it must be either a 2d or 3d array, 2d is only supported for zsize==1
		* out of range values are clipped to the range supported by the tile's dtype
		"""
		tile_dtype = self.header.dtype

		if tile_dtype == TileConstants.DTypes.UINT1:
			dtype = np.uint8
		elif tile_dtype == TileConstants.DTypes.UINT4:
			dtype = np.uint8
		elif tile_dtype == TileConstants.DTypes.UINT8:
			dtype = np.uint8
		elif tile_dtype == TileConstants.DTypes.UINT16:
			dtype = np.uint16
		elif tile_dtype == TileConstants.DTypes.BFLOAT16:
			dtype = np.float16
		else:
			raise RuntimeError("Unsupported dtype")

		dtype = np.dtype(dtype)

		if data.ndim == 2:
			xsize = data.shape[1]
			ysize = data.shape[0]
			zsize = 1
		elif data.ndim == 3:
			zsize, ysize, xsize = data.shape
		else:
			raise ValueError("Data must be 2d or 3d array")

		if self.header.xsize != xsize or self.header.ysize != ysize or self.header.zsize != zsize:
			raise ValueError("Array dimensions do not match tile header")

		if self.header.version > 4:
			# When we introduce new Tile versions, make sure that the direct
			# tile buffer dump still can be done like this.
			raise RuntimeError(f"Unsupported tile version {self.header.version}!")

		# convert the datatype from float (which we needed because we were
		# summing up fractions) to the given dtype. Then substract the valueOffset
		# from every element, making nodes that are still "-1" NULL in
		# the tile.
		# astype(): convert to desired dtype (always big endian because our Tile files store values like that by convention)
		# tobytes(): get the raw buffer back from numpy.

		if dtype.kind == 'f':
			info = np.finfo(dtype)
		else:
			info = np.iinfo(dtype)

		d_off = data - self.header.valueOffset
		common_dtype = np.promote_types(d_off.dtype, dtype)
		converted = d_off.astype(common_dtype).clip(info.min, info.max).astype(dtype.newbyteorder(">"))

		if dtype.kind == 'f':
			# for float tiles massage numpy to store all NaNs as 0xFFFF
			inttype = dtype.str.replace('f', 'i')
			view = converted.view(inttype)
			view[np.isnan(data)] = -1
		else:
			# for int tiles, 'holes' in the data are marked as 0 (because of valueOffset=-1)
			converted[np.isnan(data)] = 0

		# Manually combine values shorter than 1 byte
		# UINT1 tiles are "bitwise little endian", so they get special treatment
		# everything else is big endian
		if tile_dtype < 8:
			if 8 % tile_dtype:
				raise ValueError("tile_dtype must be integer divisor of 8")
			stride = 8 // tile_dtype

			flat = converted.view()
			flat.shape = (np.prod(converted.shape), )
			flat &= (1 << tile_dtype) - 1

			if tile_dtype == 1:
				flat <<= 7

			for i in range(1, stride):
				if tile_dtype == 1:
					flat[::stride] = flat[::stride] >> tile_dtype | flat[i::stride]
				else:
					flat[::stride] = flat[::stride] << tile_dtype | flat[i::stride]

			converted = flat[::stride]

		self.mm[self.headerOffset:] = converted.tobytes()
