import numpy as np
import pytest

from arf.Tile import Tile

@pytest.mark.parametrize("ts, expected_lat, expected_lon", [
    ("n475e0165", 47.5, 16.5),
    ("n470e0160", 47, 16),
    ("n469e0160", 46.9, 16),
    ("s300e0145", -30, 14.5),
    ("s301e0145", -30.1, 14.5),
    ("n381w0785", 38.1, -78.5),
    ("s380w0785", -38, -78.5),
    ("s379w0785", -37.9, -78.5),
    ("s381w0785", -38.1, -78.5),
    ("n380w0785", 38, -78.5),
    ("n380w0790", 38, -79),
    ("n381w0790", 38.1, -79),
    ("n380w0791", 38, -79.1),
    ("n000e0000", 0.0, 0.0),
    ("n001e0001", 0.1, 0.1),
    ("s001e0001", -0.1, 0.1),
    ("s001w0001", -0.1, -0.1),
    ("s899e0000", -89.9, 0.0),
    ("n111e0811", 11.1, 81.1),
    ("n011w0011", 1.1, -1.1),
    ("n011w0001", 1.1, -0.1),
    ("n001w0011", 0.1, -1.1),
])
def test_get_coordinates_for_tilespec(ts, expected_lat, expected_lon):
    lat, lon = Tile.getCoordinatesForTilespec(ts)
    assert lat == pytest.approx(expected_lat)
    assert lon == pytest.approx(expected_lon)
    actual_ts = Tile.generateTilespec(lat, lon)
    assert ts == actual_ts


@pytest.mark.parametrize("ts", [
    "s991e9991",
    "s991w0011",
])
def test_get_coordinates_for_tilespec_garbage(ts):
    lat, lon = Tile.getCoordinatesForTilespec(ts)
    # Behavior could change or test removed in the future, so no assertions


def test_tilespec():
    for latitude in np.arange(-90, 90, 0.1):
        for longitude in np.arange(-180, 180, 0.1):
            ts = Tile.generateTilespec(latitude, longitude)
            lat_ts, lon_ts = Tile.getCoordinatesForTilespec(ts)

            # We expect that the coordinates of the tilespec are those of the lower left corner
            # of the 0.1 x 0.1 area on which the Point (latitude, longitude) is located.
            assert lat_ts <= latitude
            assert latitude <= lat_ts + 0.1

            assert lon_ts <= longitude
            assert longitude <= lon_ts + 0.1
