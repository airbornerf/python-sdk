import numpy as np
import pytest

from arf.Tile import Tile

@pytest.mark.parametrize("ts, expected_lat, expected_lon", [
    ("n47e016", 47, 16),
    ("s30e014", -30, 14),
    ("n38w078", 38, -78),
    ("s38w078", -38, -78),
    ("s37w078", -37, -78),
    ("s38w078", -38, -78),
    ("n38w078", 38, -78),
    ("n00e000", 0, 0),
    ("s01e001", -1, 1),
    ("s01w001", -1, -1),
    ("n01w001", 1, -1),
    ("s89e000", -89, 0.0),
    ("s90e000", -90, 0.0)
])
def test_get_coordinates_for_tilespec(ts, expected_lat, expected_lon):
    lat, lon = Tile.getCoordinatesForTilespec(ts)
    assert lat == pytest.approx(expected_lat)
    assert lon == pytest.approx(expected_lon)
    actual_ts = Tile.generateTilespec1Deg(lat, lon)
    assert ts == actual_ts


@pytest.mark.parametrize("ts", [
    "s99e999",
    "s99w001",
])
def test_get_coordinates_for_tilespec_garbage(ts):
    lat, lon = Tile.getCoordinatesForTilespec(ts)
    # Behavior could change or test removed in the future, so no assertions

@pytest.mark.parametrize("lat, lon, expected_ts", [
    (0.0, 0.0, "n00e000"),
    (90.0, 180.0, "n90e000"),
    (-90.0, -180.0, "s90w180"),
    (0.1, 0.1, "n00e000"),
    (-0.1, 0.1, "s01e000"),
    (-0.1, -0.1, "s01w001"),
    (0.1, -0.1, "n00w001"),
    (1., 1., "n01e001"),
    (-1., 1., "s01e001"),
    (-1., -1., "s01w001"),
    (1., -1., "n01w001"),
    (89.1, 0.1, "n89e000"),
    (-89.1, 0.1, "s90e000"),
    (-89.1, -0.1, "s90w001"),
    (89.1, -0.1, "n89w001"),
    (89.1, 179.1, "n89e179"),
    (-89.1, 179.1, "s90e179"),
    (-89.1, -179.1, "s90w180"),
    (89.1, -179.1, "n89w180"),
    (89., 179., "n89e179"),
    (-89., 179., "s89e179"),
    (-89., -179., "s89w179"),
    (89., -179., "n89w179"),
    (-90., 179., "s90e179"),
    (-90., -179., "s90w179")
])
def test_get_one_degree_tilespec_test(lat, lon, expected_ts):
    actual_ts = Tile.generateTilespec1Deg(lat,lon)
    assert expected_ts == actual_ts

def test_tilespec_roundtrip():
    for latitude in np.arange(-90, 90, 0.5):
        for longitude in np.arange(-180, 180, 0.5):
            ts = Tile.generateTilespec1Deg(latitude, longitude)
            lat_ts, lon_ts = Tile.getCoordinatesForTilespec(ts)

            # We expect that the coordinates of the tilespec are those of the lower left corner
            # of the 1 x 1 area on which the Point (latitude, longitude) is located.
            assert lat_ts <= latitude
            assert latitude <= lat_ts + 1

            assert lon_ts <= longitude
            assert longitude <= lon_ts + 1

@pytest.mark.parametrize("expected_ts_1_deg, ts", [
    ("n00e000", "n000e0000"),
    ("n89e180", "n899e1800"),
    ("s90w180", "s900w1800"),
    ("n00e000", "n001e0001"),
    ("s01e000", "s001e0001"),
    ("s01w001", "s001w0001"),
    ("n00w001", "n001w0001"),
    ("n89e000", "n891e0001"),
    ("s90e000", "s891e0001"),
    ("s90w001", "s891w0001"),
    ("n89w001", "n891w0001"),
    ("n89e179", "n891e1791"),
    ("s90e179", "s891e1791"),
    ("s90w180", "s891w1791"),
    ("n89w180", "n891w1791"),
    ("n00e000", "n009e0009"),
    ("n89e179", "n890e1799"),
    ("s90w180", "s899w1799"),
    ("n00e000", "n009e0001"),
    ("s01e000", "s009e0001"),
    ("s01w001", "s009w0009"),
    ("n00w001", "n009w0009"),
    ("n89e000", "n899e0001"),
    ("s20e099", "s199e0999"),
    ("s79w100", "s790w0999"),
    ("n89w001", "n891w0005")
])
def test_generateOneDegreeTilespecForTilespec(expected_ts_1_deg, ts):
    assert expected_ts_1_deg == Tile.generateTilespec1DegForTilespec(ts)



